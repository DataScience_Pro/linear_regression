# README
This is Linear Regression Project.

OLS model is used.

OLS stands for Ordinary Least Squares.

## Issues
1. In order to install statsmodels package, you should install vc build Tool

## Regression Result
OLS Regression Result

![OLS_regression_result](image/OLS_regression_result.PNG)

Linear Regression Graph

![Linear Regression Graph](image/linear_regression_graph.png)

Comparison Graph

![Comparison Graph](image/comparison_graph.PNG)